# Seismic Tomography Italy 2017 Di Stefano and Ciaccio

This is a csv, pipe (|) separated file containing the P and S wave velocity model for the Italian region published in https://doi.org/10.1016/j.jog.2014.09.006 and updated in 2017 with inverted 0km depth layer.


License: https://creativecommons.org/licenses/by/4.0/deed.it
INGV web page: www.ingv.it
